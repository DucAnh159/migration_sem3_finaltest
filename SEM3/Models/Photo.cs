﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace SEM3.Models
{
    [Table("Photo")]
    public class Photo
    {
        //---------------------------PhotoID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PhotoID { get; set; }

        //---------------------------Photo Name
        public string PhotoName { get; set; }

        //---------------------------ProductID - Foreign Key reference Product
        public int ProductID { get; set; }
        [ForeignKey("ProductID")]
        public Product GetProduct { get; set; }
    }
}
