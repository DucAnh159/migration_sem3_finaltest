﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEM3.Models
{

    public class Context_DB : DbContext
    {
        public Context_DB(DbContextOptions options) : base(options) { }

        public DbSet<ContactUs> ContactUs_M { get; set; }
        public DbSet<AboutUs> AboutUs_M { get; set; }
        public DbSet<Admin> Admins_M { get; set; }
        public DbSet<User> Users_M { get; set; }
        public DbSet<Category> Categories_M { get; set; }
        public DbSet<Type> Types_M { get; set; }
        public DbSet<Shipping> Shippings_M { get; set; }
        public DbSet<Feedback> Feedbacks_M { get; set; }
        public DbSet<Product> Products_M { get; set; }
        public DbSet<Photo> Photos_M { get; set; }
        public DbSet<WishLists> WishLists_M { get; set; }
        public DbSet<AuctionHistories> AuctionHistories_M { get; set; }
        public DbSet<Invoices> Invoices_M { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //--------------------------------------------- Declare Foreign Key
            modelBuilder.Entity<Feedback>().HasKey(hk => new { hk.FeedbackID, hk.UserID });
            modelBuilder.Entity<Product>().HasKey(hk => new { hk.ProductID, hk.UserID, hk.CategoryID, hk.TypeID });
            modelBuilder.Entity<Photo>().HasKey(hk => new { hk.PhotoID, hk.ProductID });
            modelBuilder.Entity<WishLists>().HasKey(hk => new { hk.WishListsID, hk.UserID });
            modelBuilder.Entity<AuctionHistories>().HasKey(hk => new { hk.AuctionHistoriesID, hk.UserID, hk.ProductID });
            modelBuilder.Entity<Invoices>().HasKey(hk => new { hk.InvoicesID, hk.UserID, hk.ProductID, hk.ShippingID });

            //--------------------------------------------- Associalted Keys
            //Constraint a foreign key in the Feedback table from the User table
            modelBuilder.Entity<Feedback>().HasOne(ho => ho.GetUser).WithMany(wm => wm.U_Feedbacks_FK).HasForeignKey(hf => hf.UserID);

            //Constraint a foreign key in the Product table from the User table
            modelBuilder.Entity<Product>().HasOne(ho => ho.GetUser).WithMany(wm => wm.U_Product_FK).HasForeignKey(hf => hf.UserID);

            //Constraint a foreign key in the Product table from the Category table
            modelBuilder.Entity<Product>().HasOne(ho => ho.GetCategory).WithMany(wm => wm.C_Products_FK).HasForeignKey(hf => hf.CategoryID);

            //Constraint a foreign key in the Product table from the Type table
            modelBuilder.Entity<Type>().HasOne<Product>(ho => ho.T_Products_FK).WithOne(wm => wm.GetTypes).HasPrincipalKey<Type>(hr => hr.TypeID).HasForeignKey<Product>(hr => hr.TypeID);

            //Constraint a foreign key in the Photo table from the Product table
            modelBuilder.Entity<Product>().HasMany<Photo>(ho => ho.P_Photos_FK).WithOne(wm => wm.GetProduct).HasPrincipalKey(hr => hr.ProductID).HasForeignKey(hr => hr.ProductID);

            //Constraint a foreign key in the WishLists table from the User table
            modelBuilder.Entity<WishLists>().HasOne(ho => ho.GetUser).WithMany(wm => wm.U_WishLists_FK).HasForeignKey(hr => hr.UserID);

            //Constraint a foreign key in the WishLists table from the Product table
            //modelBuilder.Entity<WishLists>().HasOne(ho => ho.GetProduct).WithMany(wm => wm.P_WishLists_FK).HasPrincipalKey(hr => hr.ProductID).HasForeignKey(hr => hr.ProductID);

            //Constraint a foreign key in the AuctionHistories table from the User table
            modelBuilder.Entity<AuctionHistories>().HasOne(ho => ho.GetUser).WithMany(wm => wm.U_AuctionHistories_FK).HasForeignKey(hr => hr.UserID);

            //Constraint a foreign key in the AuctionHistories table from the Product table
            modelBuilder.Entity<Product>().HasOne<AuctionHistories>(ho => ho.P_AuctionHistories_FK).WithOne(wo => wo.GetProduct).HasPrincipalKey<Product>(hr => hr.ProductID).HasForeignKey<AuctionHistories>(hr => hr.ProductID);


            //Constraint a foreign key in the Invoices table from the User table
            modelBuilder.Entity<Invoices>().HasOne(ho => ho.GetUser).WithMany(wm => wm.U_Invoices_FK).HasForeignKey(hr => hr.UserID);

            //Constraint a foreign key in the Invoices table from the Shipping table
            modelBuilder.Entity<Invoices>().HasOne(ho => ho.GetShipping).WithMany(wm => wm.S_Invoices_FK).HasForeignKey(hr => hr.ShippingID);

            //Constraint a foreign key in the Invoices table from the Product table
            modelBuilder.Entity<Product>().HasOne<Invoices>(ho => ho.P_Invoices_FK).WithOne(wo => wo.GetProduct).HasPrincipalKey<Product>(hr => hr.ProductID).HasForeignKey<Invoices>(hr => hr.ProductID);

            //--------------------------------------------- Insert Data Seeding
            //Contact Us Table
            //modelBuilder.Entity<ContactUs>().HasData(
            //    new ContactUs { ContactUsID = 1, ContentColumnFirst = "", ContentColumnSecond = "", ContentColumnThird = "" }
            //    );
            ////About Us Table
            //modelBuilder.Entity<AboutUs>().HasData(
            //    new AboutUs { AboutUsID = 1, DescriptionTop = "", DescriptionMiddle = "", DescriptionBottom = "", ImageTop = "", ImageMiddle = "", ImageBottom = "" }
            //    );
            //Admin Table
            modelBuilder.Entity<Admin>().HasData(
                new Admin { AdminID = 1, FullName = "Nathan Nguyen", Email = "nathannguyen@gmail.com", Password = "NATHAN_Nguyen_123", Feature = "/images/nathannguyen.png", CreateAt = DateTime.Now },
                new Admin { AdminID = 2, FullName = "Quang Dang", Email = "quangdang@gmail.com", Password = "QUANG_Dang_456", Feature = "/images/quangdang.png", CreateAt = DateTime.Now },
                new Admin { AdminID = 3, FullName = "Hong Chuong", Email = "hongchuong@gmail.com", Password = "HONG_Chuong_789", Feature = "/images/hongchuong.png", CreateAt = DateTime.Now },
                new Admin { AdminID = 4, FullName = "Hoang An", Email = "hoangan@gmail.com", Password = "HOANG_An_101112", Feature = "/images/hoangan.png", CreateAt = DateTime.Now }
                );
            //User Table
            modelBuilder.Entity<User>().HasData(
                new User { UserID = 1, FullName = "Hoang Thanh", Gender = "Male", DOB = DateTime.Parse("2-11-1878"), Interest = "Painting", Email = "hoangthanh@gmail.com", Password = "HOANG_Thanh_000", PhoneNumber = "0359454521", Feature = null, Balance = 1000000000, Address = "Road RE23", City = "Long Xuyên", States = "An Giang", Country = "Vietnam", Roles = "Seller", CreateAt = DateTime.Now },
                new User { UserID = 2, FullName = "Khanh Thy", Gender = "Female", DOB = DateTime.Parse("3-10-1868"), Interest = "Carpet", Email = "khanhthy@gmail.com", Password = "KHANH_Thy_999", PhoneNumber = "0359872321", Feature = null, Balance = 2300000000, Address = "Road 153", City = "Bay Harbor Islands", States = "Florida", Country = "United States", Roles = "Buyer", CreateAt = DateTime.Now },
                new User { UserID = 3, FullName = "Tran Hoang", Gender = "Other", DOB = DateTime.Parse("4-9-1858"), Interest = "Scripture", Email = "tranhoang@gmail.com", Password = "TRAN_Hoang_888", PhoneNumber = "0313874521", Feature = null, Balance = 1200000000, Address = "Road 193", City = "Cho Dok", States = "An Giang", Country = "Vietnam", Roles = "Buyer", CreateAt = DateTime.Now },
                new User { UserID = 4, FullName = "Phuong Dung", Gender = "Female", DOB = DateTime.Parse("5-8-1988"), Interest = "Carpet", Email = "phuongdung@gmail.com", Password = "PHUONG_Dung_777", PhoneNumber = "0356874521", Feature = null, Balance = 5600000000, Address = "Road R23", City = "Eddy County", States = "New Mexico", Country = "United States", Roles = "Seller", CreateAt = DateTime.Now },
                new User { UserID = 5, FullName = "Thanh Tuyen", Gender = "Male", DOB = DateTime.Parse("6-7-1978"), Interest = "Painting", Email = "thanhtuyen@gmail.com", Password = "THANH_Tuyen_666", PhoneNumber = "0378874521", Feature = null, Balance = 7800000000, Address = "Road A23", City = "Baldwin Harbor", States = "New York", Country = "United States", Roles = "Buyer", CreateAt = DateTime.Now },
                new User { UserID = 6, FullName = "Quoc Vuong", Gender = "Other", DOB = DateTime.Parse("7-6-1998"), Interest = "Painting", Email = "quocvuong@gmail.com", Password = "QUOC_Vuong_555", PhoneNumber = "0359124521", Feature = null, Balance = 1500000000, Address = "Road 5F", City = "Pleiku", States = "Gia Lai", Country = "Vietnam", Roles = "Buyer", CreateAt = DateTime.Now },
                new User { UserID = 7, FullName = "Chi Thanh", Gender = "Male", DOB = DateTime.Parse("8-5-1983"), Interest = "Scripture", Email = "chithanh@gmail.com", Password = "CHI_Thanh_444", PhoneNumber = "0359874381", Feature = null, Balance = 7300000000, Address = "Road ED1", City = "Akasahebpet", States = "Andhra Pradesh", Country = "India", Roles = "Seller", CreateAt = DateTime.Now },
                new User { UserID = 8, FullName = "Hoang Kien", Gender = "Female", DOB = DateTime.Parse("9-4-1982"), Interest = "Carpet", Email = "hoangkien@gmail.com", Password = "HOANG_Kien_333", PhoneNumber = "0359782521", Feature = null, Balance = 2500000000, Address = "Road 1A", City = "Balod", States = "Chhattisgarh ", Country = "India", Roles = "Seller", CreateAt = DateTime.Now },
                new User { UserID = 9, FullName = "Phan Anh", Gender = "Other", DOB = DateTime.Parse("10-3-1985"), Interest = "Scripture", Email = "phananh@gmail.com", Password = "PHAN_Anh_222", PhoneNumber = "0359898121", Feature = null, Balance = 9100000000, Address = "Road 23", City = "Hanoi", States = "Hanoi", Country = "Vietnam", Roles = "Seller", CreateAt = DateTime.Now },
                new User { UserID = 10, FullName = "Hoang Ha", Gender = "Female", DOB = DateTime.Parse("1-2-1998"), Interest = "Carpet", Email = "hoangha@gmail.com", Password = "HOANG_Ha_111", PhoneNumber = "0359851421", Feature = null, Balance = 2600000000, Address = "Road 13", City = "Alhambra", States = "California", Country = "United States", Roles = "Seller", CreateAt = DateTime.Now }
                );
            //Category Table
            modelBuilder.Entity<Category>().HasData(
                new Category { CategoryID = 1, CategoryName = "Painting", CreateAt = DateTime.Now },
                new Category { CategoryID = 2, CategoryName = "Carpet", CreateAt = DateTime.Now },
                new Category { CategoryID = 3, CategoryName = "Scripture", CreateAt = DateTime.Now }
                );
            //Type Table
            modelBuilder.Entity<Type>().HasData(
                new Type { TypeID = 1, TypeName = "Not Sold Yet", Descriptions = "This artwork has not been sold yet", Status = true, CreateAt = DateTime.Now },
                new Type { TypeID = 2, TypeName = "Auction", Descriptions = "This work of art is sold by auction", Status = true, CreateAt = DateTime.Now },
                new Type { TypeID = 3, TypeName = "Buy Now", Descriptions = "This work of art is sold with an immediate purchase", Status = true, CreateAt = DateTime.Now }
                );
            //Shipping Table
            modelBuilder.Entity<Shipping>().HasData(
                new Shipping { ShippingID = 1, FullName = "Thanh Hoang", Email = "thanhhoang@gmail.com", PhoneNumber = "09253097816", Address = "Road A23", City = "Baldwin Harbor", States = "New York", Country = "United States" },
                new Shipping { ShippingID = 2, FullName = "Phuong Trang", Email = "phuongtrang@gmail.com", PhoneNumber = "08925647816", Address = "Road 13", City = "Alhambra", States = "California", Country = "United States" },
                new Shipping { ShippingID = 3, FullName = "Hong Van", Email = "hongvan@gmail.com", PhoneNumber = "09253603616", Address = "Road 5F", City = "Pleiku", States = "Gia Lai", Country = "Vietnam" },
                new Shipping { ShippingID = 4, FullName = "Quoc Thuan", Email = "quocthuan@gmail.com", PhoneNumber = "09018647816", Address = "Road 153", City = "Bay Harbor Islands", States = "Florida", Country = "United States" },
                new Shipping { ShippingID = 5, FullName = "Si Thanh", Email = "sithanh@gmail.com", PhoneNumber = "09253123816", Address = "Road 5F", City = "Pleiku", States = "Gia Lai", Country = "Vietnam" },
                new Shipping { ShippingID = 6, FullName = "Thai Vu", Email = "thaivu@gmail.com", PhoneNumber = "09253649116", Address = "Road RE23", City = "Long Xuyên", States = "An Giang", Country = "Vietnam" }
                );
            //Feedback Table
            modelBuilder.Entity<Feedback>().HasData(
                new Feedback { FeedbackID = 1, FullName = "Hoang Thanh", Email = "hoangthanh@gmail.com", Content = "Test Feedback 1", Reply = true, CreateAt = DateTime.Now, UserID = 1 },
                new Feedback { FeedbackID = 2, FullName = "Khanh Thy", Email = "khanhthy@gmail.com", Content = "Test Feedback 2", Reply = false, CreateAt = DateTime.Now, UserID = 2 },
                new Feedback { FeedbackID = 3, FullName = "Tran Hoang", Email = "tranhoang@gmail.com", Content = "Test Feedback 3", Reply = true, CreateAt = DateTime.Now, UserID = 3 },
                new Feedback { FeedbackID = 4, FullName = "Phuong Dung", Email = "phuongdung@gmail.com", Content = "Test Feedback 4", Reply = false, CreateAt = DateTime.Now, UserID = 4 }
                );
            //Product Table
            modelBuilder.Entity<Product>().HasData(
                new Product
                {
                    ProductID = 1,
                    ProductName = "Nur Ilham Faces of India",
                    Artist = "Nur Ilham",
                    BidPrice = 0,
                    FixedPrice = 0,
                    Duration = DateTime.Parse("01-01-0001"),
                    Origin = "India",
                    Condition = "Good condition",
                    ShortDescription = null,
                    LongDescription = "The paintings by Nur Ilham are very innovative and of a very high quality.<br/>His paintings are distinguished by his detailed painting technique, which makes his paintings and watercolours very realistic, < br />they have the appearance of a photograph.",
                    Status = false,
                    CreateAt = DateTime.Now,
                    UserID = 1,
                    CategoryID = 1,
                    TypeID = 1
                },
                new Product
                {
                    ProductID = 2,
                    ProductName = "Mata Ni Pachedi",
                    Artist = "Sanjay.M.Chitara Mata-ni-Pachedi",
                    BidPrice = 160,
                    FixedPrice = 0,
                    Duration = DateTime.Parse("2-11-2016"),
                    Origin = "India",
                    Condition = "Good condition",
                    ShortDescription = null,
                    LongDescription = "77 X 62 In | 196 X 157 Cm<br/> Vegetable Dye on Cotton < br />Folk < br />Created in 2020 < br />Sold by Gallery, Shipped Rolled unless rolling not possible < br /> Lot No 288466",
                    Status = false,
                    CreateAt = DateTime.Now,
                    UserID = 1,
                    CategoryID = 1,
                    TypeID = 2
                },
                    new Product
                    {
                        ProductID = 3,
                        ProductName = "Nur Ilham - Faces of India",
                        Artist = "Nur Ilham",
                        BidPrice = 50,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("2-11-2017"),
                        Origin = "India",
                        Condition = "Good condition",
                        ShortDescription = null,
                        LongDescription = "Nur Ilham paintings are distinguished by his detailed painting technique, which makes his paintings and watercolours very realistic, <br/> they have the appearance of a photograph.He specialises in painting bird species and portraits, including the collection Owls,African tribes,< br />Birds of prey,and Geisha.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 4,
                        CategoryID = 1,
                        TypeID = 2
                    },
                    new Product
                    {
                        ProductID = 4,
                        ProductName = "Drawing (1) - Ink, Paper, pencil - Miniatura di Raja su carta - India - 19th century",
                        Artist = "Miniatura di Raja su carta",
                        BidPrice = 0,
                        FixedPrice = 10000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Good condition, see description",
                        ShortDescription = null,
                        LongDescription = "The sketch on paper represents a Raja who, intent on smoking a water pipe, holds a hawk resting on his arm. The scene takes place in the presence of a woman and a girl.<br/>The sketch is interesting from a collector's point of view because it acts as a matrix for other designs. The shape of the Raja appears perforated by holes; applying pencil or charcoal powder on the sheet and letting it pass through the cracks, the same image was imprinted on the sheet below. The technique is called 'a spolvero'.<br/> Good state of preservation and rare.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 7,
                        CategoryID = 1,
                        TypeID = 3
                    },
                    new Product
                    {
                        ProductID = 5,
                        ProductName = "George Heidweiller - Zonder titel",
                        Artist = "George Heidweiller",
                        BidPrice = 70,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("2-2-2017"),
                        Origin = "India and the USA",
                        Condition = "Good condition",
                        ShortDescription = null,
                        LongDescription = "This diptych (2 pieces of 30 x 30 cm) was made by the artist George Heidweiller based on his experiences in India and the USA, painted with acrylic paint and mixed media on linen.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 10,
                        CategoryID = 1,
                        TypeID = 2
                    },
                    new Product
                    {
                        ProductID = 6,
                        ProductName = "Sarouck - Carpet - 228 cm - 166 cm",
                        Artist = "None",
                        BidPrice = 0,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "good condition",
                        ShortDescription = "Wool on cotton - India - 21st century<br/><br/>On offer is a beautiful Sarouk Mir oriental carpet.Very beautiful colours and pattern.< br />Please look at the carpet with patience and attention.< br />Each handmade carpet is unique in its design, beauty and colour harmony and there for a work of art in itself.< br />< br /> ",
                        LongDescription = "On offer is a beautiful Sarouk Mir oriental carpet<br/>Very beautiful colours and pattern < br />Please look at the carpet with patience and attention.< br />Each handmade carpet is unique in its design,< br />beauty and colour harmony and therefore < br />a work of art in itself.< br />< br />Pile: 100 % new wool<br/>Warp: 100 % cotton < br />Finish: 100 % hand - knotted < br />Condition: good condition < br />Length: 228 cm < br />Width: 166 cm < br />Material: Wool on cotton < br />Country of Origin: India < br />Manufacturing period: 21st century",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 10,
                        CategoryID = 2,
                        TypeID = 1
                    },
                    new Product
                    {
                        ProductID = 7,
                        ProductName = "Kaschmir - Carpet - 230 cm - 145 cm",
                        Artist = "None",
                        BidPrice = 0,
                        FixedPrice = 48000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Overall in good condition",
                        ShortDescription = "Silk on cotton - India - Late 20th century<br/>The silk rugs from Kashmir are hand - knotted in India according to the design of Persian rugs.The oriental patterns of the rugs consist of medallions or repetitions of patterns, and also the depiction of the tree of life.",
                        LongDescription = "The silk rugs from Kashmir are hand-knotted in India according to the design of Persian rugs. The oriental patterns of the rugs consist of medallions or repetitions of patterns, and also the depiction of the tree of life. The rugs are very well crafted, and the quality wool gives them finesse and lightness, with a lovely shiny appearance. The use of quality wool also makes it possible to create very delicate patterns and one cannot fail to be impressed by the work involved in the making of such a carpet. ",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 9,
                        CategoryID = 2,
                        TypeID = 3
                    },
                    new Product
                    {
                        ProductID = 8,
                        ProductName = "Ghoum - Carpet - 303 cm - 217 cm",
                        Artist = "None",
                        BidPrice = 120,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("2-11-2017"),
                        Origin = "India",
                        Condition = "Overall in good condition",
                        ShortDescription = "These pieces are works of art of the highest quality in both material and handicraft.<br/> Please look at the carpet with patience and attention.Each handmade carpet is unique in its design,beauty and colour harmony and therefore a work of art in itself.",
                        LongDescription = "These pieces are works of art of the highest quality in both material and handicraft.<br/> Please look at the carpet with patience and attention.Each handmade carpet is unique in its design, beauty and colour harmony and therefore a work of art in itself.< br />We guarantee that above carpet is a genuine hand - knotted oriental carpet.< br />A guaranteed hand - knotted Qom carpet.< br />Origin: India < br />Province: Qom < br />Pile: 100 % wool < br />Warp: 100 % cotton < br />Length: 303 cm < br />Width: 217 cm < br />Material: Wool on cotton < br />Condition: in good overall condition.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 8,
                        CategoryID = 2,
                        TypeID = 2
                    },
                    new Product
                    {
                        ProductID = 9,
                        ProductName = "Gabbeh - Runner - 315 cm - 85 cm",
                        Artist = "None",
                        BidPrice = 90,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("2-11-2018"),
                        Origin = "India",
                        Condition = "Overall in good condition",
                        ShortDescription = "Wool on wool - India - Late 20th century<br/>On offer is a beautiful, hand - knotted < br />Gabbeh - from beautiful India.",
                        LongDescription = "Wool on wool - India - Late 20th century<br/> On offer is a beautiful, hand - knotted < br />Gabbeh < br />from beautiful India.< br />Please look at the carpet with patience and attention.< br /> Province: Gabbeh < br />Made in India < br />Dimensions: 85x315cm < br />Wool on wool < br />Auction number: 2549 < br />Length: 315 cm < br />Width: 85 cm < br />Material: Wool on wool < br />Including certificate of authenticity.< br />< br />The carpet is in good condition overall,see pictures < br />We recommend enlarging the high resolution pictures to get a detailed impression.< br />< br />The carpet was exported from Iran before 01 / 01 / 2015.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 7,
                        CategoryID = 2,
                        TypeID = 2
                    },
                    new Product
                    {
                        ProductID = 10,
                        ProductName = "Bidjar - Carpet - 163 cm - 92 cm",
                        Artist = "None",
                        BidPrice = 0,
                        FixedPrice = 800000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "good condition",
                        ShortDescription = "These pieces are works of art of the highest quality in both material and handicraft.<br/><br/>Please look at the carpet with patience and attention.Each handmade carpet is unique in its design, beauty and colour harmony and therefore a work of art in itself.",
                        LongDescription = "These pieces are works of art of the highest quality in both material and handicraft.<br/><br/>Please look at the carpet with patience and attention.Each handmade carpet is unique in its design,beauty and colour harmony and therefore a work of art in itself.< br />< br />We guarantee that the above carpet is a genuine hand - knotted oriental carpet.< br />< br />A guaranteed hand - knotted carpet, Bijar < br />Origin: India < br />Province: Bidjar < br />Length: 163 cm < br />Width: 92 cm < br />Pile: 100 % wool < br />Warp: 100 % cotton < br />Manufacturing period: 21st century < br />Condition: good condition < br />Insured shipping with GLS.< br />Internal number: 993 < br />With tracking number to follow the shipment.< br /> ",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 1,
                        CategoryID = 2,
                        TypeID = 3
                    },

                    new Product
                    {
                        ProductID = 11,
                        ProductName = "Zes jaren uit het leven van Wemmervan Berchem",
                        Artist = "W.S. Caine",
                        BidPrice = 0,
                        FixedPrice = 120000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Good",
                        ShortDescription = null,
                        LongDescription = "Mr. L.C.D. van Dijk - Zes jaren uit het leven van Wemmer van Berchem, gevolgd door iets over onze vroegste betrekkingen met Japan. Two historical contributions. - Amsterdam, J.H. Scheltema, 1858. Illustrated with 2 facsimiles of letters from Van Berchem. [VIII], XII, 82, {II], 42 pp. - Sewn, original cover (22.5 x 14.3 cm.).< br >Condition: copy from the Library of Parliament,with some small stamps.Spine of the binding reinforced with foil.Except for that a good copy.Very rare.< br >*The relatively unknown Wemmer van Berchem was a Dutch skipper / merchant who made trips to Cuba, South America, Africa and Asia in the early 17th century, where he worked for the Dutch East India Company.The second contribution in the book is a description of the very earliest Dutch relations with Japan in the period between 1585 and 1644.Will be shipped registered.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 4,
                        CategoryID = 3,
                        TypeID = 3
                    },
                    new Product
                    {
                        ProductID = 12,
                        ProductName = "Pictoresque India",
                        Artist = "W.S. Caine",
                        BidPrice = 0,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Good",
                        ShortDescription = null,
                        LongDescription = "Beautiful book with gilt edges and two maps. In good to very good condition, but with very slight, hardly visible moisture damage at the bottom, see pictures. The first, little-used map is unfortunately torn, but it can be easily restored. The second map is as new, only one quire is slightly torn. The condition of the book is otherwise good, except for some minor bumps to the spine and corners, not worth mentioning. Some scattered minor foxing. Beautiful copy.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 4,
                        CategoryID = 3,
                        TypeID = 1
                    },
                    new Product
                    {
                        ProductID = 13,
                        ProductName = "Les Princesses Malabares ou Le Célibat Phylosophique",
                        Artist = "Louis Pierre de Longue",
                        BidPrice = 900,
                        FixedPrice = 0,
                        Duration = DateTime.Parse("2-11-2019"),
                        Origin = "India",
                        Condition = "Good",
                        ShortDescription = null,
                        LongDescription = "First edition which was condemned to be burnt.< br >'Man naturally throws himself on everything that is forbidden to him. The Turk loves wine because his law forbids him to drink it. We are eager to solicit sex, because we are directed to not even consider a woman for fear that the glance might stir up desires detrimental to marital fidelity.'< br >This story takes place in India and is narrated by an Indian prince.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 4,
                        CategoryID = 3,
                        TypeID = 2
                    },
                    new Product
                    {
                        ProductID = 14,
                        ProductName = "L'Histoire Des Religions de tous les Royaumes du Monde",
                        Artist = "Jovet Chanoine De Laon",
                        BidPrice = 0,
                        FixedPrice = 300000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Good",
                        ShortDescription = null,
                        LongDescription = "Fascinating; De la religion de l'Assyrie, de la Turcomanie ou grande Arménie, des Armeniens, de la Gerogie ou Gurgistan, de l'Albanie, de l'Avogasie & de la Circassie, du Curdistan, de la Perse, des Gavres, de l'île d'Ormuz, de l'Empire du Grand Mongol, des royaumes des Guzarate, de Brampour, de Delly, de Decan & Cunkan, de l'île de Goa, des îles de Choran & de Divar de la presqu'île de l'Inde au delà le Golfe de Bengala, de la côte de Coromandel, des Chrétiens de S. ",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 10,
                        CategoryID = 3,
                        TypeID = 3
                    },
                    new Product
                    {
                        ProductID = 15,
                        ProductName = "Au Paradis des Rajahs",
                        Artist = "André de Fouquières",
                        BidPrice = 0,
                        FixedPrice = 10000,
                        Duration = DateTime.Parse("01-01-0001"),
                        Origin = "India",
                        Condition = "Good",
                        ShortDescription = null,
                        LongDescription = "Preface by Henri Lavedan. Handwritten dedication by the author.Stunning work illustrated < br >with photographs of India at the turn of the 20th century.Very good overall condition.",
                        Status = false,
                        CreateAt = DateTime.Now,
                        UserID = 1,
                        CategoryID = 3,
                        TypeID = 3
                    }
                );
            //Photo Table
            modelBuilder.Entity<Photo>().HasData(
                new Photo { PhotoID = 1, PhotoName = "/images/products/Nur_Ilham_Faces_of_India_img1.png", ProductID = 1 },
                new Photo { PhotoID = 2, PhotoName = "/images/products/Nur_Ilham_Faces_of_India_img2.png", ProductID = 1 },
                new Photo { PhotoID = 3, PhotoName = "/images/products/Nur_Ilham_Faces_of_India_img3.png", ProductID = 1 },
                new Photo { PhotoID = 4, PhotoName = "/images/products/Nur_Ilham_Faces_of_India_img4.png", ProductID = 1 },
                new Photo { PhotoID = 5, PhotoName = "/images/products/Nur_Ilham_Faces_of_India_img5.png", ProductID = 1 },

                new Photo { PhotoID = 6, PhotoName = "/images/products/Mata_Ni_Pachedi_img1.png", ProductID = 2 },
                new Photo { PhotoID = 7, PhotoName = "/images/products/Mata_Ni_Pachedi_img2.png", ProductID = 2 },
                new Photo { PhotoID = 8, PhotoName = "/images/products/Mata_Ni_Pachedi_img3.png", ProductID = 2 },
                new Photo { PhotoID = 9, PhotoName = "/images/products/Mata_Ni_Pachedi_img4.png", ProductID = 2 },
                new Photo { PhotoID = 10, PhotoName = "/images/products/Mata_Ni_Pachedi_img5.png", ProductID = 2 },

                new Photo { PhotoID = 11, PhotoName = "/images/products/George_Heidweiller_Zonder_titel_img1.png", ProductID = 3 },
                new Photo { PhotoID = 12, PhotoName = "/images/products/George_Heidweiller_Zonder_titel_img2.png", ProductID = 3 },
                new Photo { PhotoID = 13, PhotoName = "/images/products/George_Heidweiller_Zonder_titel_img3.png", ProductID = 3 },
                new Photo { PhotoID = 14, PhotoName = "/images/products/George_Heidweiller_Zonder_titel_img4.png", ProductID = 3 },
                new Photo { PhotoID = 15, PhotoName = "/images/products/George_Heidweiller_Zonder_titel_img5.png", ProductID = 3 },

                new Photo { PhotoID = 16, PhotoName = "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img1.png", ProductID = 4 },
                new Photo { PhotoID = 17, PhotoName = "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img2.png", ProductID = 4 },
                new Photo { PhotoID = 18, PhotoName = "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img3.png", ProductID = 4 },
                new Photo { PhotoID = 19, PhotoName = "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img4.png", ProductID = 4 },
                new Photo { PhotoID = 20, PhotoName = "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img5.png", ProductID = 4 },

                new Photo { PhotoID = 21, PhotoName = "/images/products/Anton_Heyboer_Abstracte_compositie_img1.png", ProductID = 5 },
                new Photo { PhotoID = 22, PhotoName = "/images/products/Anton_Heyboer_Abstracte_compositie_img2.png", ProductID = 5 },
                new Photo { PhotoID = 23, PhotoName = "/images/products/Anton_Heyboer_Abstracte_compositie_img3.png", ProductID = 5 },
                new Photo { PhotoID = 24, PhotoName = "/images/products/Anton_Heyboer_Abstracte_compositie_img4.png", ProductID = 5 },
                new Photo { PhotoID = 25, PhotoName = "/images/products/Anton_Heyboer_Abstracte_compositie_img5.png", ProductID = 5 },

                new Photo { PhotoID = 26, PhotoName = "/images/products/Sarouck_Carpet_228cm_166cm_img1.jpg", ProductID = 6 },
                new Photo { PhotoID = 27, PhotoName = "/images/products/Sarouck_Carpet_228cm_166cm_img2.jpg", ProductID = 6 },
                new Photo { PhotoID = 28, PhotoName = "/images/products/Sarouck_Carpet_228cm_166cm_img3.jpg", ProductID = 6 },
                new Photo { PhotoID = 29, PhotoName = "/images/products/Sarouck_Carpet_228cm_166cm_img4.jpg", ProductID = 6 },
                new Photo { PhotoID = 30, PhotoName = "/images/products/Sarouck_Carpet_228cm_166cm_img5.jpg", ProductID = 6 },

                new Photo { PhotoID = 31, PhotoName = "/images/products/Kaschmir_Carpet_230cm_145cm_img1.jpg", ProductID = 7 },
                new Photo { PhotoID = 32, PhotoName = "/images/products/Kaschmir_Carpet_230cm_145cm_img2.jpg", ProductID = 7 },
                new Photo { PhotoID = 33, PhotoName = "/images/products/Kaschmir_Carpet_230cm_145cm_img3.jpg", ProductID = 7 },
                new Photo { PhotoID = 34, PhotoName = "/images/products/Kaschmir_Carpet_230cm_145cm_img4.jpg", ProductID = 7 },
                new Photo { PhotoID = 35, PhotoName = "/images/products/Kaschmir_Carpet_230cm_145cm_img5.jpg", ProductID = 7 },

                new Photo { PhotoID = 36, PhotoName = "/images/products/Ghoum_Carpet_303cm_217cm_img1.jpg", ProductID = 8 },
                new Photo { PhotoID = 37, PhotoName = "/images/products/Ghoum_Carpet_303cm_217cm_img2.jpg", ProductID = 8 },
                new Photo { PhotoID = 38, PhotoName = "/images/products/Ghoum_Carpet_303cm_217cm_img3.jpg", ProductID = 8 },
                new Photo { PhotoID = 39, PhotoName = "/images/products/Ghoum_Carpet_303cm_217cm_img4.jpg", ProductID = 8 },
                new Photo { PhotoID = 40, PhotoName = "/images/products/Ghoum_Carpet_303cm_217cm_img5.jpg", ProductID = 8 },

                new Photo { PhotoID = 41, PhotoName = "/images/products/Gabbeh_Runner_315cm_85cm_img1.jpg", ProductID = 9 },
                new Photo { PhotoID = 42, PhotoName = "/images/products/Gabbeh_Runner_315cm_85cm_img2.jpg", ProductID = 9 },
                new Photo { PhotoID = 43, PhotoName = "/images/products/Gabbeh_Runner_315cm_85cm_img3.jpg", ProductID = 9 },
                new Photo { PhotoID = 44, PhotoName = "/images/products/Gabbeh_Runner_315cm_85cm_img4.jpg", ProductID = 9 },
                new Photo { PhotoID = 45, PhotoName = "/images/products/Gabbeh_Runner_315cm_85cm_img5.jpg", ProductID = 9 },

                new Photo { PhotoID = 46, PhotoName = "/images/products/Bidjar_Carpet_163cm_92cm_img1.jpg", ProductID = 10 },
                new Photo { PhotoID = 47, PhotoName = "/images/products/Bidjar_Carpet_163cm_92cm_img2.jpg", ProductID = 10 },
                new Photo { PhotoID = 48, PhotoName = "/images/products/Bidjar_Carpet_163cm_92cm_img3.jpg", ProductID = 10 },
                new Photo { PhotoID = 49, PhotoName = "/images/products/Bidjar_Carpet_163cm_92cm_img4.jpg", ProductID = 10 },
                new Photo { PhotoID = 50, PhotoName = "/images/products/Bidjar_Carpet_163cm_92cm_img5.jpg", ProductID = 10 },

                new Photo { PhotoID = 51, PhotoName = "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img1.jpg", ProductID = 11 },
                new Photo { PhotoID = 52, PhotoName = "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img2.jpg", ProductID = 11 },
                new Photo { PhotoID = 53, PhotoName = "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img3.jpg", ProductID = 11 },
                new Photo { PhotoID = 54, PhotoName = "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img4.jpg", ProductID = 11 },
                new Photo { PhotoID = 55, PhotoName = "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img5.jpg", ProductID = 11 },

                new Photo { PhotoID = 56, PhotoName = "/images/products/pictoresqueIndia_img1.jpg", ProductID = 12 },
                new Photo { PhotoID = 57, PhotoName = "/images/products/pictoresqueIndia_img2.jpg", ProductID = 12 },
                new Photo { PhotoID = 58, PhotoName = "/images/products/pictoresqueIndia_img3.jpg", ProductID = 12 },
                new Photo { PhotoID = 59, PhotoName = "/images/products/pictoresqueIndia_img4.jpg", ProductID = 12 },
                new Photo { PhotoID = 60, PhotoName = "/images/products/pictoresqueIndia_img5.jpg", ProductID = 12 },

                new Photo { PhotoID = 61, PhotoName = "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img1.jpg", ProductID = 13 },
                new Photo { PhotoID = 62, PhotoName = "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img2.jpg", ProductID = 13 },
                new Photo { PhotoID = 63, PhotoName = "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img3.jpg", ProductID = 13 },
                new Photo { PhotoID = 64, PhotoName = "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img4.jpg", ProductID = 13 },
                new Photo { PhotoID = 65, PhotoName = "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img5.jpg", ProductID = 13 },

                new Photo { PhotoID = 66, PhotoName = "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img1.jpg", ProductID = 14 },
                new Photo { PhotoID = 67, PhotoName = "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img2.jpg", ProductID = 14 },
                new Photo { PhotoID = 68, PhotoName = "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img3.jpg", ProductID = 14 },
                new Photo { PhotoID = 69, PhotoName = "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img4.jpg", ProductID = 14 },
                new Photo { PhotoID = 70, PhotoName = "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img5.jpg", ProductID = 14 },

                new Photo { PhotoID = 71, PhotoName = "/images/products/AuParadisdesRajahs_img1.jpg", ProductID = 15 },
                new Photo { PhotoID = 72, PhotoName = "/images/products/AuParadisdesRajahs_img2.jpg", ProductID = 15 },
                new Photo { PhotoID = 73, PhotoName = "/images/products/AuParadisdesRajahs_img3.jpg", ProductID = 15 },
                new Photo { PhotoID = 74, PhotoName = "/images/products/AuParadisdesRajahs_img4.jpg", ProductID = 15 },
                new Photo { PhotoID = 75, PhotoName = "/images/products/AuParadisdesRajahs_img5.jpg", ProductID = 15 }
                );
            //WishLists Table
            modelBuilder.Entity<WishLists>().HasData(
                new WishLists { WishListsID = 1, Artist = "Na Nguyen", Origin = "India", Condition = "Good", Description = "No Descriptions", Status = false, CreateAt = DateTime.Now, UserID = 1 }
                );
            //AuctionHistories Table
            modelBuilder.Entity<AuctionHistories>().HasData(
                new AuctionHistories { AuctionHistoriesID = 1, Bid = 110, Status = false, CreateAt = DateTime.Now, UserID = 10, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 2, Bid = 160, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 3, Bid = 180, Status = false, CreateAt = DateTime.Now, UserID = 6, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 4, Bid = 250, Status = false, CreateAt = DateTime.Now, UserID = 4, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 5, Bid = 300, Status = false, CreateAt = DateTime.Now, UserID = 8, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 6, Bid = 400, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 7, Bid = 550, Status = false, CreateAt = DateTime.Now, UserID = 1, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 8, Bid = 880, Status = false, CreateAt = DateTime.Now, UserID = 9, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 9, Bid = 960, Status = false, CreateAt = DateTime.Now, UserID = 3, ProductID = 9 },
                new AuctionHistories { AuctionHistoriesID = 10, Bid = 1100, Status = true, CreateAt = DateTime.Now, UserID = 10, ProductID = 9 },

                new AuctionHistories { AuctionHistoriesID = 11, Bid = 180, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 12, Bid = 230, Status = false, CreateAt = DateTime.Now, UserID = 10, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 13, Bid = 500, Status = false, CreateAt = DateTime.Now, UserID = 6, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 14, Bid = 550, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 15, Bid = 600, Status = false, CreateAt = DateTime.Now, UserID = 4, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 16, Bid = 700, Status = false, CreateAt = DateTime.Now, UserID = 7, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 17, Bid = 750, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 18, Bid = 800, Status = false, CreateAt = DateTime.Now, UserID = 8, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 19, Bid = 830, Status = false, CreateAt = DateTime.Now, UserID = 2, ProductID = 2 },
                new AuctionHistories { AuctionHistoriesID = 20, Bid = 1080, Status = true, CreateAt = DateTime.Now, UserID = 9, ProductID = 2 }
                );
            //Invoices Table
            modelBuilder.Entity<Invoices>().HasData(
                //Bid
                new Invoices { InvoicesID = 1, SellerName = "Hoang Thanh", BuyerName = "Phan Anh", Note = null, Status = true, CreateAt = DateTime.Now, UserID = 9, ProductID = 2, ShippingID = 4 },
                new Invoices { InvoicesID = 2, SellerName = "Chi Thanh", BuyerName = "Hoang Ha", Note = null, Status = true, CreateAt = DateTime.Now, UserID = 10, ProductID = 9, ShippingID = 2 },
                //Buy Now
                new Invoices { InvoicesID = 3, SellerName = "Hoang Thanh", BuyerName = "Chi Thanh", Note = null, Status = false, CreateAt = DateTime.Now, UserID = 10, ProductID = 10, ShippingID = 1 },
                new Invoices { InvoicesID = 4, SellerName = "Hoang Ha", BuyerName = "Thanh Tuyen", Note = null, Status = true, CreateAt = DateTime.Now, UserID = 5, ProductID = 14, ShippingID = 5 },
                new Invoices { InvoicesID = 5, SellerName = "Chi Thanh", BuyerName = "Tran Hoang", Note = null, Status = false, CreateAt = DateTime.Now, UserID = 3, ProductID = 4, ShippingID = 3 },
                new Invoices { InvoicesID = 6, SellerName = "Phan Anh", BuyerName = "Quoc Vuong", Note = null, Status = true, CreateAt = DateTime.Now, UserID = 6, ProductID = 7, ShippingID = 6 }
                );
        }
    }
}
