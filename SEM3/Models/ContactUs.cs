﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("ContactUs")]
    public class ContactUs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactUsID { get; set; }


        [Required(ErrorMessage = "Description cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Description 1 to 1700 characters")]
        [DefaultValue("No Description available")]
        public string Description { get; set; }


        [Required(ErrorMessage = "Content Column First cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Content Column First 1 to 1700 characters")]
        [DefaultValue("No Content Column First available")]
        public string ContentColumnFirst { get; set; }


        [Required(ErrorMessage = "Content Column Second cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Content Column Second 1 to 1700 characters")]
        [DefaultValue("No Content Column Second available")]
        public string ContentColumnSecond { get; set; }


        [Required(ErrorMessage = "Content Column Third cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Content Column Third 1 to 1700 characters")]
        [DefaultValue("No Content Column Third available")]
        public string ContentColumnThird { get; set; }
    }
}
