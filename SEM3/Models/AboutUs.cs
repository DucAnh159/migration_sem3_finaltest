﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("AboutUs")]
    public class AboutUs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AboutUsID { get; set; }

        [Required(ErrorMessage = "Description Top cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Description Top 1 to 1700 characters")]
        [DefaultValue("No Description Top available")]
        public string DescriptionTop { get; set; }

        [Required(ErrorMessage = "Description Middle cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Description Middle 1 to 1700 characters")]
        [DefaultValue("No Description Middle available")]
        public string DescriptionMiddle { get; set; }

        [Required(ErrorMessage = "Description Bottom cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Description Bottom 1 to 1700 characters")]
        [DefaultValue("No Description Bottom available")]
        public string DescriptionBottom { get; set; }

        public string ImageTop { get; set; }

        public string ImageMiddle { get; set; }
        
        public string ImageBottom { get; set; }
    }
}
