﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SEM3.Models
{
    [Table("Type")]
    public class Type
    {
        private DateTime now;

        //---------------------------TypeID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TypeID { get; set; }

        //---------------------------Type Name
        [Required(ErrorMessage = "Type cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Type 1 to 150 characters")]
        public string TypeName { get; set; }

        //---------------------------Descriptions
        [DataType(DataType.MultilineText)]
        [StringLength(700, MinimumLength = 1, ErrorMessage = "Type 1 to 700 characters")]
        [DefaultValue("No description available")]
        public string Descriptions { get; set; }

        //---------------------------Status
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Status { get; set; }

        //---------------------------CreateAt
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------The keys are referenced from the Type table
        public virtual Product T_Products_FK { get; set; }
    }
}
