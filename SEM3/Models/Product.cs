﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("Product")]
    public class Product
    {
        private DateTime now;

        //---------------------------ProductID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID { get; set; }

        //---------------------------Product Name
        [Required(ErrorMessage = "Product Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Product Name 1 to 150 characters")]
        public string ProductName { get; set; }

        //---------------------------Artist
        [Required(ErrorMessage = "Artist cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Artist 1 to 150 characters")]
        public string Artist { get; set; }

        //---------------------------Bid Price
        [DataType(DataType.Currency)]
        [Range(0,1000000000000, ErrorMessage = "Bid Price must be entered between 0 and 1000000000000")]
        [DefaultValue(0)]
        public double BidPrice { get; set; }

        //---------------------------Fixed Price
        [DataType(DataType.Currency)]
        [Range(0, 1000000000000, ErrorMessage = "Fixed Price must be entered between 0 and 1000000000000")]
        [DefaultValue(0)]
        public double FixedPrice { get; set; }

        //---------------------------Duration
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        [DefaultValue(null)]
        public DateTime Duration { get; set; }

        //---------------------------Origin
        [Required(ErrorMessage = "Origin cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Origin 1 to 150 characters")]
        public string Origin { get; set; }

        //---------------------------Condition
        [Required(ErrorMessage = "Condition cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Condition 1 to 150 characters")]
        public string Condition { get; set; }

        //---------------------------Short Description
        [DataType(DataType.MultilineText)]
        [StringLength(700, MinimumLength = 1, ErrorMessage = "Short Description 1 to 700 characters")]
        [DefaultValue("No short description available")]
        public string ShortDescription {get; set;}

        //---------------------------Long Description
        [DataType(DataType.MultilineText)]
        [StringLength(1700, MinimumLength = 1, ErrorMessage = "Long Description 1 to 1700 characters")]
        [DefaultValue("No long description available")]
        public string LongDescription { get; set; }

        //---------------------------Status
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Status { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }


        //---------------------------UserID - Foreign Key reference User
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User GetUser { get; set; }

        //---------------------------CategoryID - Foreign Key reference Category
        public int CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public Category GetCategory { get; set; }

        //---------------------------TypeID - Foreign Key reference Type
        public int TypeID { get; set; }
        [ForeignKey("TypeID")]
        public Type GetTypes { get; set; }

        //---------------------------The keys are referenced from the Product table
        //public virtual ICollection<WishLists> P_WishLists_FK { get; set; }
        public virtual AuctionHistories P_AuctionHistories_FK { get; set; }
        public virtual Invoices P_Invoices_FK { get; set; }
        public virtual ICollection<Photo> P_Photos_FK { get; set; }
    }
}
