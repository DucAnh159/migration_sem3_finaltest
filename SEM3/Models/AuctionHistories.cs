﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("AuctionHistories")]
    public class AuctionHistories
    {
        private DateTime now;

        //---------------------------AuctionHistoriesID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuctionHistoriesID { get; set; }

        //---------------------------Bid
        [Required(ErrorMessage = "Bid cannot be blank")] 
        [DataType(DataType.Currency)]
        [Range(0, 1000000000000, ErrorMessage = "Bid must be entered between 0 and 1000000000000")]
        [DefaultValue(0)]
        public double Bid { get; set; }

        //---------------------------Status
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Status { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------UserID - Foreign Key reference User
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User GetUser { get; set; }

        //---------------------------ProductID - Foreign Key reference Product
        public int ProductID { get; set; }
        [ForeignKey("ProductID")]
        public Product GetProduct { get; set; }
    }
}
