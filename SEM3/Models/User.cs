﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("User")]
    public class User
    {
        private DateTime now;

        //---------------------------UserID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserID { get; set; }

        //---------------------------FullName
        [Required(ErrorMessage = "Full Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Full Name 1 to 150 characters")]
        public string FullName { get; set; }

        //---------------------------Gender
        [Required(ErrorMessage = "Gender cannot be blank")]
        [DefaultValue("Male")]
        [RegularExpression("(Male|Female|Other)")]
        public string Gender { get; set; }

        //---------------------------Date Of Birthday
        [Required(ErrorMessage = "Date Of Birthday cannot be blank")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        //---------------------------Interest
        [Required(ErrorMessage = "Interest cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(350, MinimumLength = 1, ErrorMessage = "Interest 1 to 350 characters")]
        [DefaultValue("No interest available")]
        public string Interest { get; set; }

        //---------------------------Email
        [Required(ErrorMessage = "Email cannot be blank")]
        [DataType(DataType.EmailAddress)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Email 1 to 150 characters")]
        [RegularExpression("^[A-Za-z0-9._%+-]*@[A-Za-z0-9.-]*\\.[A-Za-z0-9-]{2,}$", ErrorMessage = "Email is required and must be properly formatted.")]
        public string Email { get; set; }

        //---------------------------Password
        [Required(ErrorMessage = "Password cannot be blank")]
        [DataType(DataType.Password)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Password 1 to 150 characters")]
        [RegularExpression("^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$", ErrorMessage = "At least one upper case English letter , At least one lower case English letter, At least one digit, At least one special character, Minimum eight in length")]
        public string Password { get; set; }

        //---------------------------Phone Number
        [Required(ErrorMessage = "PhoneNumber cannot be blank")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(12, MinimumLength = 10, ErrorMessage = "Phone Number is from 10 to 12 numeric characters")]
        [RegularExpression("^[0-9]{10,12}$", ErrorMessage = "Phone is required and must be properly formatted.")]
        public string PhoneNumber { get; set; }

        //---------------------------Feature
        public string Feature { get; set; }

        //---------------------------Balance
        [Required(ErrorMessage = "Balance cannot be blank")]
        [DataType(DataType.Currency)]
        [Range(0, 1000000000000000, ErrorMessage = "Balance must be entered between 0 and 1000000000000000")]
        [DefaultValue(0)]
        public double Balance { get; set; }

        //---------------------------Address
        [Required(ErrorMessage = "Address cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Address is from 1 to 150 characters")]
        public string Address { get; set; }

        //---------------------------City
        [Required(ErrorMessage = "City cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "City is from 1 to 150 characters")]
        public string City { get; set; }

        //---------------------------States
        [Required(ErrorMessage = "States cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "States is from 1 to 150 characters")]
        public string States { get; set; }

        //---------------------------Country
        [Required(ErrorMessage = "Country cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Country is from 1 to 150 characters")]
        public string Country { get; set; }

        //---------------------------Roles
        [Display(Name = "Roles")]
        [DefaultValue("Buyers")]
        [RegularExpression("(Seller|Buyers)")]
        public string Roles { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------The keys are referenced from the User table
        public virtual ICollection<Product> U_Product_FK { get; set; }
        public virtual ICollection<WishLists> U_WishLists_FK { get; set; }
        public virtual ICollection<AuctionHistories> U_AuctionHistories_FK { get; set; }
        public virtual ICollection<Invoices> U_Invoices_FK { get; set; }
        public virtual ICollection<Feedback> U_Feedbacks_FK { get; set; }
    }
}
