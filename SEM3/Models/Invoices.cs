﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("Invoices")]
    public class Invoices
    {
        private DateTime now;

        //---------------------------InvoicesID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InvoicesID { get; set; }

        //---------------------------Seller Name
        [Required(ErrorMessage = "Seller Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Seller Name 1 to 150 characters")]
        public string SellerName { get; set; }

        //---------------------------Buyer Name
        [Required(ErrorMessage = "Buyer Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Buyer Name 1 to 150 characters")]
        public string BuyerName { get; set; }

        //---------------------------Note
        [DataType(DataType.MultilineText)]
        [StringLength(700, MinimumLength = 1, ErrorMessage = "Note 1 to 700 characters")]
        [DefaultValue("No note available")]
        public string Note { get; set; }

        //---------------------------Status
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Status { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------UserID - Foreign Key reference User
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User GetUser { get; set; }

        //---------------------------ProductID - Foreign Key reference Product
        public int ProductID { get; set; }
        [ForeignKey("ProductID")]
        public Product GetProduct { get; set; }

        //---------------------------ShippingID - Foreign Key reference Shipping
        public int ShippingID { get; set; }
        [ForeignKey("ShippingID")]
        public Shipping GetShipping { get; set; }
    }
}
