﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("WishLists")]
    public class WishLists
    {
        private DateTime now;

        //---------------------------WishListsID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WishListsID { get; set; }

        //---------------------------Artist
        [Required(ErrorMessage = "Artist cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Artist 1 to 150 characters")]
        public string Artist { get; set; }

        //---------------------------Origin
        [Required(ErrorMessage = "Origin cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Origin 1 to 150 characters")]
        public string Origin { get; set; }

        //---------------------------Condition
        [Required(ErrorMessage = "Condition cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Condition 1 to 150 characters")]
        public string Condition { get; set; }

        //---------------------------Description
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        [StringLength(700, MinimumLength = 1, ErrorMessage = "Description 1 to 700 characters")]
        [DefaultValue("No description available")]
        public string Description { get; set; }

        //---------------------------Status
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Status { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------UserID - Foreign Key reference User
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User GetUser { get; set; }

        //---------------------------ProductID - Foreign Key reference Product
        //public int? ProductID { get; set; }
        //[ForeignKey("ProductID")]
        //public Product GetProduct { get; set; }
    }
}
