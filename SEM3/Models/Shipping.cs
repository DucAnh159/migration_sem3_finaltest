﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("Shipping")]
    public class Shipping
    {
        //---------------------------ShippingID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShippingID { get; set; }

        //---------------------------Full Name
        [Required(ErrorMessage = "Full Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Full Name 1 to 150 characters")]
        public string FullName { get; set; }

        //---------------------------Email
        [Required(ErrorMessage = "Email cannot be blank")]
        [DataType(DataType.EmailAddress)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Email 1 to 150 characters")]
        [RegularExpression("^[A-Za-z0-9._%+-]*@[A-Za-z0-9.-]*\\.[A-Za-z0-9-]{2,}$", ErrorMessage = "Email is required and must be properly formatted.")]
        public string Email { get; set; }

        //---------------------------Phone Number
        [Required(ErrorMessage = "PhoneNumber cannot be blank")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(12, MinimumLength = 10, ErrorMessage = "Phone Number is from 10 to 12 numeric characters")]
        [RegularExpression("^[0-9]{10,12}$", ErrorMessage = "Phone is required and must be properly formatted.")]
        public string PhoneNumber { get; set; }

        //---------------------------Address
        [Required(ErrorMessage = "Address cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Address is from 1 to 150 characters")]
        public string Address { get; set; }

        //---------------------------City
        [Required(ErrorMessage = "City cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "City is from 1 to 150 characters")]
        public string City { get; set; }

        //---------------------------States
        [Required(ErrorMessage = "States cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "States is from 1 to 150 characters")]
        public string States { get; set; }

        //---------------------------Country
        [Required(ErrorMessage = "Country cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Country is from 1 to 150 characters")]
        public string Country { get; set; }

        //---------------------------The keys are referenced from the Shipping table
        public virtual ICollection<Invoices> S_Invoices_FK { get; set; }
    }
}
