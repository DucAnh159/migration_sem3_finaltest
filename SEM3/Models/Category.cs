﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace SEM3.Models
{
    [Table("Category")]
    public class Category
    {
        private DateTime now;

        //---------------------------CategoryID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }

        //---------------------------Category Name
        [Required(ErrorMessage = "Category Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Category Name 1 to 150 characters")]
        public string CategoryName { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------The keys are referenced from the Category table
        public virtual ICollection<Product> C_Products_FK { get; set; }
    }
}
