﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("Feedback")]
    public class Feedback
    {
        private DateTime now;

        //---------------------------FeedbackID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FeedbackID { get; set; }

        //---------------------------Full Name
        [Required(ErrorMessage = "Full Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Full Name 1 to 150 characters")]
        public string FullName { get; set; }

        //---------------------------Email
        [Required(ErrorMessage = "Email cannot be blank")]
        [DataType(DataType.EmailAddress)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Email 1 to 150 characters")]
        [RegularExpression("^[A-Za-z0-9._%+-]*@[A-Za-z0-9.-]*\\.[A-Za-z0-9-]{2,}$", ErrorMessage = "Email is required and must be properly formatted.")]
        public string Email { get; set; }

        //---------------------------Content
        [Required(ErrorMessage = "Content cannot be blank")]
        [DataType(DataType.MultilineText)]
        [StringLength(700, MinimumLength = 1, ErrorMessage = "Content 1 to 700 characters")]
        [DefaultValue("No content available")]
        public string Content { get; set; }

        //---------------------------Reply
        [DefaultValue(false)]
        [RegularExpression("(true|false)")]
        public bool Reply { get; set; }

        //---------------------------Create At
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }

        //---------------------------UserID - Foreign Key reference User
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User GetUser { get; set; }
        
    }
}
