﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEM3.Models
{
    [Table("Admin")]
    public class Admin
    {
        private DateTime now;

        //---------------------------AdminID - Primary Key
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdminID { get; set; }

        //---------------------------FullName
        [Required(ErrorMessage = "Full Name cannot be blank")]
        [DataType(DataType.Text)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Full Name 1 to 150 characters")]
        public string FullName { get; set; }

        //---------------------------Email
        [Required(ErrorMessage = "Email cannot be blank")]
        [DataType(DataType.EmailAddress)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Email 1 to 150 characters")]
        [RegularExpression("^[A-Za-z0-9._%+-]*@[A-Za-z0-9.-]*\\.[A-Za-z0-9-]{2,}$", ErrorMessage = "Email is required and must be properly formatted.")]
        public string Email { get; set; }

        //---------------------------Password
        [Required(ErrorMessage = "Password cannot be blank")]
        [DataType(DataType.Password)]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Password 1 to 150 characters")]
        [RegularExpression("^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$", ErrorMessage = "At least one upper case English letter , At least one lower case English letter, At least one digit, At least one special character, Minimum eight in length")]
        public string Password { get; set; }

        //---------------------------Feature
        public string Feature { get; set; }

        //---------------------------CreateAt
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateAt
        {
            get { return now; }
            set { now = DateTime.Now; }
        }
    }
}
