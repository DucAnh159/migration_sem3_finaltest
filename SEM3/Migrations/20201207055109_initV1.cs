﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SEM3.Migrations
{
    public partial class initV1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AboutUs",
                columns: table => new
                {
                    AboutUsID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DescriptionTop = table.Column<string>(maxLength: 1700, nullable: false),
                    DescriptionMiddle = table.Column<string>(maxLength: 1700, nullable: false),
                    DescriptionBottom = table.Column<string>(maxLength: 1700, nullable: false),
                    ImageTop = table.Column<string>(nullable: true),
                    ImageMiddle = table.Column<string>(nullable: true),
                    ImageBottom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AboutUs", x => x.AboutUsID);
                });

            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    AdminID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Email = table.Column<string>(maxLength: 150, nullable: false),
                    Password = table.Column<string>(maxLength: 150, nullable: false),
                    Feature = table.Column<string>(nullable: true),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.AdminID);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    CategoryID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CategoryName = table.Column<string>(maxLength: 150, nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "ContactUs",
                columns: table => new
                {
                    ContactUsID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 1700, nullable: false),
                    ContentColumnFirst = table.Column<string>(maxLength: 1700, nullable: false),
                    ContentColumnSecond = table.Column<string>(maxLength: 1700, nullable: false),
                    ContentColumnThird = table.Column<string>(maxLength: 1700, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUs", x => x.ContactUsID);
                });

            migrationBuilder.CreateTable(
                name: "Shipping",
                columns: table => new
                {
                    ShippingID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Email = table.Column<string>(maxLength: 150, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 12, nullable: false),
                    Address = table.Column<string>(maxLength: 150, nullable: false),
                    City = table.Column<string>(maxLength: 150, nullable: false),
                    States = table.Column<string>(maxLength: 150, nullable: false),
                    Country = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipping", x => x.ShippingID);
                });

            migrationBuilder.CreateTable(
                name: "Type",
                columns: table => new
                {
                    TypeID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TypeName = table.Column<string>(maxLength: 150, nullable: false),
                    Descriptions = table.Column<string>(maxLength: 700, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Type", x => x.TypeID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Gender = table.Column<string>(nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    Interest = table.Column<string>(maxLength: 350, nullable: false),
                    Email = table.Column<string>(maxLength: 150, nullable: false),
                    Password = table.Column<string>(maxLength: 150, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 12, nullable: false),
                    Feature = table.Column<string>(nullable: true),
                    Balance = table.Column<double>(nullable: false),
                    Address = table.Column<string>(maxLength: 150, nullable: false),
                    City = table.Column<string>(maxLength: 150, nullable: false),
                    States = table.Column<string>(maxLength: 150, nullable: false),
                    Country = table.Column<string>(maxLength: 150, nullable: false),
                    Roles = table.Column<string>(nullable: true),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "Feedback",
                columns: table => new
                {
                    FeedbackID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Email = table.Column<string>(maxLength: 150, nullable: false),
                    Content = table.Column<string>(maxLength: 700, nullable: false),
                    Reply = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedback", x => new { x.FeedbackID, x.UserID });
                    table.ForeignKey(
                        name: "FK_Feedback_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false),
                    TypeID = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(maxLength: 150, nullable: false),
                    Artist = table.Column<string>(maxLength: 150, nullable: false),
                    BidPrice = table.Column<double>(nullable: false),
                    FixedPrice = table.Column<double>(nullable: false),
                    Duration = table.Column<DateTime>(nullable: false),
                    Origin = table.Column<string>(maxLength: 150, nullable: false),
                    Condition = table.Column<string>(maxLength: 150, nullable: false),
                    ShortDescription = table.Column<string>(maxLength: 700, nullable: true),
                    LongDescription = table.Column<string>(maxLength: 1700, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => new { x.ProductID, x.UserID, x.CategoryID, x.TypeID });
                    table.UniqueConstraint("AK_Product_ProductID", x => x.ProductID);
                    table.ForeignKey(
                        name: "FK_Product_Category_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Category",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Product_Type_TypeID",
                        column: x => x.TypeID,
                        principalTable: "Type",
                        principalColumn: "TypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Product_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WishLists",
                columns: table => new
                {
                    WishListsID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    Artist = table.Column<string>(maxLength: 150, nullable: false),
                    Origin = table.Column<string>(maxLength: 150, nullable: false),
                    Condition = table.Column<string>(maxLength: 150, nullable: false),
                    Description = table.Column<string>(maxLength: 700, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WishLists", x => new { x.WishListsID, x.UserID });
                    table.ForeignKey(
                        name: "FK_WishLists_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuctionHistories",
                columns: table => new
                {
                    AuctionHistoriesID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    ProductID = table.Column<int>(nullable: false),
                    Bid = table.Column<double>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuctionHistories", x => new { x.AuctionHistoriesID, x.UserID, x.ProductID });
                    table.ForeignKey(
                        name: "FK_AuctionHistories_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuctionHistories_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    InvoicesID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    ProductID = table.Column<int>(nullable: false),
                    ShippingID = table.Column<int>(nullable: false),
                    SellerName = table.Column<string>(maxLength: 150, nullable: false),
                    BuyerName = table.Column<string>(maxLength: 150, nullable: false),
                    Note = table.Column<string>(maxLength: 700, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CreateAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => new { x.InvoicesID, x.UserID, x.ProductID, x.ShippingID });
                    table.ForeignKey(
                        name: "FK_Invoices_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invoices_Shipping_ShippingID",
                        column: x => x.ShippingID,
                        principalTable: "Shipping",
                        principalColumn: "ShippingID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invoices_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Photo",
                columns: table => new
                {
                    PhotoID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductID = table.Column<int>(nullable: false),
                    PhotoName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photo", x => new { x.PhotoID, x.ProductID });
                    table.ForeignKey(
                        name: "FK_Photo_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "AdminID", "CreateAt", "Email", "Feature", "FullName", "Password" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 12, 7, 12, 51, 8, 990, DateTimeKind.Local).AddTicks(9279), "nathannguyen@gmail.com", "/images/nathannguyen.png", "Nathan Nguyen", "NATHAN_Nguyen_123" },
                    { 2, new DateTime(2020, 12, 7, 12, 51, 9, 68, DateTimeKind.Local).AddTicks(4629), "quangdang@gmail.com", "/images/quangdang.png", "Quang Dang", "QUANG_Dang_456" },
                    { 3, new DateTime(2020, 12, 7, 12, 51, 9, 68, DateTimeKind.Local).AddTicks(7907), "hongchuong@gmail.com", "/images/hongchuong.png", "Hong Chuong", "HONG_Chuong_789" },
                    { 4, new DateTime(2020, 12, 7, 12, 51, 9, 68, DateTimeKind.Local).AddTicks(8321), "hoangan@gmail.com", "/images/hoangan.png", "Hoang An", "HOANG_An_101112" }
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "CategoryID", "CategoryName", "CreateAt" },
                values: new object[,]
                {
                    { 1, "Painting", new DateTime(2020, 12, 7, 12, 51, 9, 94, DateTimeKind.Local).AddTicks(3719) },
                    { 2, "Carpet", new DateTime(2020, 12, 7, 12, 51, 9, 97, DateTimeKind.Local).AddTicks(5924) },
                    { 3, "Scripture", new DateTime(2020, 12, 7, 12, 51, 9, 97, DateTimeKind.Local).AddTicks(6311) }
                });

            migrationBuilder.InsertData(
                table: "Shipping",
                columns: new[] { "ShippingID", "Address", "City", "Country", "Email", "FullName", "PhoneNumber", "States" },
                values: new object[,]
                {
                    { 5, "Road 5F", "Pleiku", "Vietnam", "sithanh@gmail.com", "Si Thanh", "09253123816", "Gia Lai" },
                    { 4, "Road 153", "Bay Harbor Islands", "United States", "quocthuan@gmail.com", "Quoc Thuan", "09018647816", "Florida" },
                    { 6, "Road RE23", "Long Xuyên", "Vietnam", "thaivu@gmail.com", "Thai Vu", "09253649116", "An Giang" },
                    { 2, "Road 13", "Alhambra", "United States", "phuongtrang@gmail.com", "Phuong Trang", "08925647816", "California" },
                    { 1, "Road A23", "Baldwin Harbor", "United States", "thanhhoang@gmail.com", "Thanh Hoang", "09253097816", "New York" },
                    { 3, "Road 5F", "Pleiku", "Vietnam", "hongvan@gmail.com", "Hong Van", "09253603616", "Gia Lai" }
                });

            migrationBuilder.InsertData(
                table: "Type",
                columns: new[] { "TypeID", "CreateAt", "Descriptions", "Status", "TypeName" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 12, 7, 12, 51, 9, 196, DateTimeKind.Local).AddTicks(8440), "This artwork has not been sold yet", true, "Not Sold Yet" },
                    { 2, new DateTime(2020, 12, 7, 12, 51, 9, 203, DateTimeKind.Local).AddTicks(204), "This work of art is sold by auction", true, "Auction" },
                    { 3, new DateTime(2020, 12, 7, 12, 51, 9, 203, DateTimeKind.Local).AddTicks(1039), "This work of art is sold with an immediate purchase", true, "Buy Now" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "UserID", "Address", "Balance", "City", "Country", "CreateAt", "DOB", "Email", "Feature", "FullName", "Gender", "Interest", "Password", "PhoneNumber", "Roles", "States" },
                values: new object[,]
                {
                    { 9, "Road 23", 9100000000.0, "Hanoi", "Vietnam", new DateTime(2020, 12, 7, 12, 51, 9, 235, DateTimeKind.Local).AddTicks(1645), new DateTime(1985, 10, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "phananh@gmail.com", null, "Phan Anh", "Other", "Scripture", "PHAN_Anh_222", "0359898121", "Seller", "Hanoi" },
                    { 1, "Road RE23", 1000000000.0, "Long Xuyên", "Vietnam", new DateTime(2020, 12, 7, 12, 51, 9, 210, DateTimeKind.Local).AddTicks(1924), new DateTime(1878, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "hoangthanh@gmail.com", null, "Hoang Thanh", "Male", "Painting", "HOANG_Thanh_000", "0359454521", "Seller", "An Giang" },
                    { 2, "Road 153", 2300000000.0, "Bay Harbor Islands", "United States", new DateTime(2020, 12, 7, 12, 51, 9, 232, DateTimeKind.Local).AddTicks(815), new DateTime(1868, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "khanhthy@gmail.com", null, "Khanh Thy", "Female", "Carpet", "KHANH_Thy_999", "0359872321", "Buyer", "Florida" },
                    { 3, "Road 193", 1200000000.0, "Cho Dok", "Vietnam", new DateTime(2020, 12, 7, 12, 51, 9, 232, DateTimeKind.Local).AddTicks(3309), new DateTime(1858, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "tranhoang@gmail.com", null, "Tran Hoang", "Other", "Scripture", "TRAN_Hoang_888", "0313874521", "Buyer", "An Giang" },
                    { 4, "Road R23", 5600000000.0, "Eddy County", "United States", new DateTime(2020, 12, 7, 12, 51, 9, 234, DateTimeKind.Local).AddTicks(4987), new DateTime(1988, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "phuongdung@gmail.com", null, "Phuong Dung", "Female", "Carpet", "PHUONG_Dung_777", "0356874521", "Seller", "New Mexico" },
                    { 5, "Road A23", 7800000000.0, "Baldwin Harbor", "United States", new DateTime(2020, 12, 7, 12, 51, 9, 234, DateTimeKind.Local).AddTicks(7128), new DateTime(1978, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "thanhtuyen@gmail.com", null, "Thanh Tuyen", "Male", "Painting", "THANH_Tuyen_666", "0378874521", "Buyer", "New York" },
                    { 6, "Road 5F", 1500000000.0, "Pleiku", "Vietnam", new DateTime(2020, 12, 7, 12, 51, 9, 234, DateTimeKind.Local).AddTicks(8040), new DateTime(1998, 7, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "quocvuong@gmail.com", null, "Quoc Vuong", "Other", "Painting", "QUOC_Vuong_555", "0359124521", "Buyer", "Gia Lai" },
                    { 7, "Road ED1", 7300000000.0, "Akasahebpet", "India", new DateTime(2020, 12, 7, 12, 51, 9, 234, DateTimeKind.Local).AddTicks(9274), new DateTime(1983, 8, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "chithanh@gmail.com", null, "Chi Thanh", "Male", "Scripture", "CHI_Thanh_444", "0359874381", "Seller", "Andhra Pradesh" },
                    { 8, "Road 1A", 2500000000.0, "Balod", "India", new DateTime(2020, 12, 7, 12, 51, 9, 235, DateTimeKind.Local).AddTicks(487), new DateTime(1982, 9, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "hoangkien@gmail.com", null, "Hoang Kien", "Female", "Carpet", "HOANG_Kien_333", "0359782521", "Seller", "Chhattisgarh " },
                    { 10, "Road 13", 2600000000.0, "Alhambra", "United States", new DateTime(2020, 12, 7, 12, 51, 9, 235, DateTimeKind.Local).AddTicks(3236), new DateTime(1998, 1, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "hoangha@gmail.com", null, "Hoang Ha", "Female", "Carpet", "HOANG_Ha_111", "0359851421", "Seller", "California" }
                });

            migrationBuilder.InsertData(
                table: "Feedback",
                columns: new[] { "FeedbackID", "UserID", "Content", "CreateAt", "Email", "FullName", "Reply" },
                values: new object[,]
                {
                    { 1, 1, "Test Feedback 1", new DateTime(2020, 12, 7, 12, 51, 9, 99, DateTimeKind.Local).AddTicks(6367), "hoangthanh@gmail.com", "Hoang Thanh", true },
                    { 2, 2, "Test Feedback 2", new DateTime(2020, 12, 7, 12, 51, 9, 105, DateTimeKind.Local).AddTicks(782), "khanhthy@gmail.com", "Khanh Thy", false },
                    { 3, 3, "Test Feedback 3", new DateTime(2020, 12, 7, 12, 51, 9, 105, DateTimeKind.Local).AddTicks(1234), "tranhoang@gmail.com", "Tran Hoang", true },
                    { 4, 4, "Test Feedback 4", new DateTime(2020, 12, 7, 12, 51, 9, 105, DateTimeKind.Local).AddTicks(1483), "phuongdung@gmail.com", "Phuong Dung", false }
                });

            migrationBuilder.InsertData(
                table: "Product",
                columns: new[] { "ProductID", "UserID", "CategoryID", "TypeID", "Artist", "BidPrice", "Condition", "CreateAt", "Duration", "FixedPrice", "LongDescription", "Origin", "ProductName", "ShortDescription", "Status" },
                values: new object[,]
                {
                    { 5, 10, 1, 2, "George Heidweiller", 70.0, "Good condition", new DateTime(2020, 12, 7, 12, 51, 9, 179, DateTimeKind.Local).AddTicks(5714), new DateTime(2017, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "This diptych (2 pieces of 30 x 30 cm) was made by the artist George Heidweiller based on his experiences in India and the USA, painted with acrylic paint and mixed media on linen.", "India and the USA", "George Heidweiller - Zonder titel", null, false },
                    { 7, 9, 2, 3, "None", 0.0, "Overall in good condition", new DateTime(2020, 12, 7, 12, 51, 9, 179, DateTimeKind.Local).AddTicks(9133), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 48000.0, "The silk rugs from Kashmir are hand-knotted in India according to the design of Persian rugs. The oriental patterns of the rugs consist of medallions or repetitions of patterns, and also the depiction of the tree of life. The rugs are very well crafted, and the quality wool gives them finesse and lightness, with a lovely shiny appearance. The use of quality wool also makes it possible to create very delicate patterns and one cannot fail to be impressed by the work involved in the making of such a carpet. ", "India", "Kaschmir - Carpet - 230 cm - 145 cm", "Silk on cotton - India - Late 20th century<br/>The silk rugs from Kashmir are hand - knotted in India according to the design of Persian rugs.The oriental patterns of the rugs consist of medallions or repetitions of patterns, and also the depiction of the tree of life.", false },
                    { 8, 8, 2, 2, "None", 120.0, "Overall in good condition", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(1125), new DateTime(2017, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "These pieces are works of art of the highest quality in both material and handicraft.<br/> Please look at the carpet with patience and attention.Each handmade carpet is unique in its design, beauty and colour harmony and therefore a work of art in itself.< br />We guarantee that above carpet is a genuine hand - knotted oriental carpet.< br />A guaranteed hand - knotted Qom carpet.< br />Origin: India < br />Province: Qom < br />Pile: 100 % wool < br />Warp: 100 % cotton < br />Length: 303 cm < br />Width: 217 cm < br />Material: Wool on cotton < br />Condition: in good overall condition.", "India", "Ghoum - Carpet - 303 cm - 217 cm", "These pieces are works of art of the highest quality in both material and handicraft.<br/> Please look at the carpet with patience and attention.Each handmade carpet is unique in its design,beauty and colour harmony and therefore a work of art in itself.", false },
                    { 9, 7, 2, 2, "None", 90.0, "Overall in good condition", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(2813), new DateTime(2018, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "Wool on wool - India - Late 20th century<br/> On offer is a beautiful, hand - knotted < br />Gabbeh < br />from beautiful India.< br />Please look at the carpet with patience and attention.< br /> Province: Gabbeh < br />Made in India < br />Dimensions: 85x315cm < br />Wool on wool < br />Auction number: 2549 < br />Length: 315 cm < br />Width: 85 cm < br />Material: Wool on wool < br />Including certificate of authenticity.< br />< br />The carpet is in good condition overall,see pictures < br />We recommend enlarging the high resolution pictures to get a detailed impression.< br />< br />The carpet was exported from Iran before 01 / 01 / 2015.", "India", "Gabbeh - Runner - 315 cm - 85 cm", "Wool on wool - India - Late 20th century<br/>On offer is a beautiful, hand - knotted < br />Gabbeh - from beautiful India.", false },
                    { 4, 7, 1, 3, "Miniatura di Raja su carta", 0.0, "Good condition, see description", new DateTime(2020, 12, 7, 12, 51, 9, 179, DateTimeKind.Local).AddTicks(3687), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000.0, "The sketch on paper represents a Raja who, intent on smoking a water pipe, holds a hawk resting on his arm. The scene takes place in the presence of a woman and a girl.<br/>The sketch is interesting from a collector's point of view because it acts as a matrix for other designs. The shape of the Raja appears perforated by holes; applying pencil or charcoal powder on the sheet and letting it pass through the cracks, the same image was imprinted on the sheet below. The technique is called 'a spolvero'.<br/> Good state of preservation and rare.", "India", "Drawing (1) - Ink, Paper, pencil - Miniatura di Raja su carta - India - 19th century", null, false },
                    { 13, 4, 3, 2, "Louis Pierre de Longue", 900.0, "Good", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(9954), new DateTime(2019, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "First edition which was condemned to be burnt.< br >'Man naturally throws himself on everything that is forbidden to him. The Turk loves wine because his law forbids him to drink it. We are eager to solicit sex, because we are directed to not even consider a woman for fear that the glance might stir up desires detrimental to marital fidelity.'< br >This story takes place in India and is narrated by an Indian prince.", "India", "Les Princesses Malabares ou Le Célibat Phylosophique", null, false },
                    { 12, 4, 3, 1, "W.S. Caine", 0.0, "Good", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(8311), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "Beautiful book with gilt edges and two maps. In good to very good condition, but with very slight, hardly visible moisture damage at the bottom, see pictures. The first, little-used map is unfortunately torn, but it can be easily restored. The second map is as new, only one quire is slightly torn. The condition of the book is otherwise good, except for some minor bumps to the spine and corners, not worth mentioning. Some scattered minor foxing. Beautiful copy.", "India", "Pictoresque India", null, false },
                    { 3, 4, 1, 2, "Nur Ilham", 50.0, "Good condition", new DateTime(2020, 12, 7, 12, 51, 9, 179, DateTimeKind.Local).AddTicks(1492), new DateTime(2017, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "Nur Ilham paintings are distinguished by his detailed painting technique, which makes his paintings and watercolours very realistic, <br/> they have the appearance of a photograph.He specialises in painting bird species and portraits, including the collection Owls,African tribes,< br />Birds of prey,and Geisha.", "India", "Nur Ilham - Faces of India", null, false },
                    { 6, 10, 2, 1, "None", 0.0, "good condition", new DateTime(2020, 12, 7, 12, 51, 9, 179, DateTimeKind.Local).AddTicks(7411), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "On offer is a beautiful Sarouk Mir oriental carpet<br/>Very beautiful colours and pattern < br />Please look at the carpet with patience and attention.< br />Each handmade carpet is unique in its design,< br />beauty and colour harmony and therefore < br />a work of art in itself.< br />< br />Pile: 100 % new wool<br/>Warp: 100 % cotton < br />Finish: 100 % hand - knotted < br />Condition: good condition < br />Length: 228 cm < br />Width: 166 cm < br />Material: Wool on cotton < br />Country of Origin: India < br />Manufacturing period: 21st century", "India", "Sarouck - Carpet - 228 cm - 166 cm", "Wool on cotton - India - 21st century<br/><br/>On offer is a beautiful Sarouk Mir oriental carpet.Very beautiful colours and pattern.< br />Please look at the carpet with patience and attention.< br />Each handmade carpet is unique in its design, beauty and colour harmony and there for a work of art in itself.< br />< br /> ", false },
                    { 15, 1, 3, 3, "André de Fouquières", 0.0, "Good", new DateTime(2020, 12, 7, 12, 51, 9, 181, DateTimeKind.Local).AddTicks(3372), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000.0, "Preface by Henri Lavedan. Handwritten dedication by the author.Stunning work illustrated < br >with photographs of India at the turn of the 20th century.Very good overall condition.", "India", "Au Paradis des Rajahs", null, false },
                    { 10, 1, 2, 3, "None", 0.0, "good condition", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(4834), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 800000.0, "These pieces are works of art of the highest quality in both material and handicraft.<br/><br/>Please look at the carpet with patience and attention.Each handmade carpet is unique in its design,beauty and colour harmony and therefore a work of art in itself.< br />< br />We guarantee that the above carpet is a genuine hand - knotted oriental carpet.< br />< br />A guaranteed hand - knotted carpet, Bijar < br />Origin: India < br />Province: Bidjar < br />Length: 163 cm < br />Width: 92 cm < br />Pile: 100 % wool < br />Warp: 100 % cotton < br />Manufacturing period: 21st century < br />Condition: good condition < br />Insured shipping with GLS.< br />Internal number: 993 < br />With tracking number to follow the shipment.< br /> ", "India", "Bidjar - Carpet - 163 cm - 92 cm", "These pieces are works of art of the highest quality in both material and handicraft.<br/><br/>Please look at the carpet with patience and attention.Each handmade carpet is unique in its design, beauty and colour harmony and therefore a work of art in itself.", false },
                    { 2, 1, 1, 2, "Sanjay.M.Chitara Mata-ni-Pachedi", 160.0, "Good condition", new DateTime(2020, 12, 7, 12, 51, 9, 175, DateTimeKind.Local).AddTicks(3118), new DateTime(2016, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "77 X 62 In | 196 X 157 Cm<br/> Vegetable Dye on Cotton < br />Folk < br />Created in 2020 < br />Sold by Gallery, Shipped Rolled unless rolling not possible < br /> Lot No 288466", "India", "Mata Ni Pachedi", null, false },
                    { 1, 1, 1, 1, "Nur Ilham", 0.0, "Good condition", new DateTime(2020, 12, 7, 12, 51, 9, 131, DateTimeKind.Local).AddTicks(454), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0.0, "The paintings by Nur Ilham are very innovative and of a very high quality.<br/>His paintings are distinguished by his detailed painting technique, which makes his paintings and watercolours very realistic, < br />they have the appearance of a photograph.", "India", "Nur Ilham Faces of India", null, false },
                    { 11, 4, 3, 3, "W.S. Caine", 0.0, "Good", new DateTime(2020, 12, 7, 12, 51, 9, 180, DateTimeKind.Local).AddTicks(6644), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 120000.0, "Mr. L.C.D. van Dijk - Zes jaren uit het leven van Wemmer van Berchem, gevolgd door iets over onze vroegste betrekkingen met Japan. Two historical contributions. - Amsterdam, J.H. Scheltema, 1858. Illustrated with 2 facsimiles of letters from Van Berchem. [VIII], XII, 82, {II], 42 pp. - Sewn, original cover (22.5 x 14.3 cm.).< br >Condition: copy from the Library of Parliament,with some small stamps.Spine of the binding reinforced with foil.Except for that a good copy.Very rare.< br >*The relatively unknown Wemmer van Berchem was a Dutch skipper / merchant who made trips to Cuba, South America, Africa and Asia in the early 17th century, where he worked for the Dutch East India Company.The second contribution in the book is a description of the very earliest Dutch relations with Japan in the period between 1585 and 1644.Will be shipped registered.", "India", "Zes jaren uit het leven van Wemmervan Berchem", null, false },
                    { 14, 10, 3, 3, "Jovet Chanoine De Laon", 0.0, "Good", new DateTime(2020, 12, 7, 12, 51, 9, 181, DateTimeKind.Local).AddTicks(1573), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 300000.0, "Fascinating; De la religion de l'Assyrie, de la Turcomanie ou grande Arménie, des Armeniens, de la Gerogie ou Gurgistan, de l'Albanie, de l'Avogasie & de la Circassie, du Curdistan, de la Perse, des Gavres, de l'île d'Ormuz, de l'Empire du Grand Mongol, des royaumes des Guzarate, de Brampour, de Delly, de Decan & Cunkan, de l'île de Goa, des îles de Choran & de Divar de la presqu'île de l'Inde au delà le Golfe de Bengala, de la côte de Coromandel, des Chrétiens de S. ", "India", "L'Histoire Des Religions de tous les Royaumes du Monde", null, false }
                });

            migrationBuilder.InsertData(
                table: "WishLists",
                columns: new[] { "WishListsID", "UserID", "Artist", "Condition", "CreateAt", "Description", "Origin", "Status" },
                values: new object[] { 1, 1, "Na Nguyen", "Good", new DateTime(2020, 12, 7, 12, 51, 9, 239, DateTimeKind.Local).AddTicks(2323), "No Descriptions", "India", false });

            migrationBuilder.InsertData(
                table: "AuctionHistories",
                columns: new[] { "AuctionHistoriesID", "UserID", "ProductID", "Bid", "CreateAt", "Status" },
                values: new object[,]
                {
                    { 5, 8, 9, 300.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(7762), false },
                    { 10, 10, 9, 1100.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(9081), true },
                    { 9, 3, 9, 960.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(8787), false },
                    { 8, 9, 9, 880.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(8554), false },
                    { 7, 1, 9, 550.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(8316), false },
                    { 6, 2, 9, 400.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(7999), false },
                    { 4, 4, 9, 250.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(7492), false },
                    { 1, 10, 9, 110.0, new DateTime(2020, 12, 7, 12, 51, 9, 71, DateTimeKind.Local).AddTicks(4947), false },
                    { 19, 2, 2, 830.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(1294), false },
                    { 18, 8, 2, 800.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(1061), false },
                    { 17, 2, 2, 750.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(829), false },
                    { 20, 9, 2, 1080.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(1587), true },
                    { 15, 4, 2, 600.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(306), false },
                    { 2, 2, 9, 160.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(6034), false },
                    { 16, 7, 2, 700.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(535), false },
                    { 11, 2, 2, 180.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(9320), false },
                    { 3, 6, 9, 180.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(7123), false },
                    { 13, 6, 2, 500.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(9773), false },
                    { 14, 2, 2, 550.0, new DateTime(2020, 12, 7, 12, 51, 9, 93, DateTimeKind.Local).AddTicks(66), false },
                    { 12, 10, 2, 230.0, new DateTime(2020, 12, 7, 12, 51, 9, 92, DateTimeKind.Local).AddTicks(9547), false }
                });

            migrationBuilder.InsertData(
                table: "Invoices",
                columns: new[] { "InvoicesID", "UserID", "ProductID", "ShippingID", "BuyerName", "CreateAt", "Note", "SellerName", "Status" },
                values: new object[,]
                {
                    { 6, 6, 7, 6, "Quoc Vuong", new DateTime(2020, 12, 7, 12, 51, 9, 116, DateTimeKind.Local).AddTicks(9735), null, "Phan Anh", true },
                    { 1, 9, 2, 4, "Phan Anh", new DateTime(2020, 12, 7, 12, 51, 9, 107, DateTimeKind.Local).AddTicks(6845), null, "Hoang Thanh", true },
                    { 4, 5, 14, 5, "Thanh Tuyen", new DateTime(2020, 12, 7, 12, 51, 9, 116, DateTimeKind.Local).AddTicks(9046), null, "Hoang Ha", true },
                    { 3, 10, 10, 1, "Chi Thanh", new DateTime(2020, 12, 7, 12, 51, 9, 116, DateTimeKind.Local).AddTicks(8714), null, "Hoang Thanh", false },
                    { 5, 3, 4, 3, "Tran Hoang", new DateTime(2020, 12, 7, 12, 51, 9, 116, DateTimeKind.Local).AddTicks(9428), null, "Chi Thanh", false },
                    { 2, 10, 9, 2, "Hoang Ha", new DateTime(2020, 12, 7, 12, 51, 9, 116, DateTimeKind.Local).AddTicks(8175), null, "Chi Thanh", true }
                });

            migrationBuilder.InsertData(
                table: "Photo",
                columns: new[] { "PhotoID", "ProductID", "PhotoName" },
                values: new object[,]
                {
                    { 36, 8, "/images/products/Ghoum_Carpet_303cm_217cm_img1.jpg" },
                    { 45, 9, "/images/products/Gabbeh_Runner_315cm_85cm_img5.jpg" },
                    { 44, 9, "/images/products/Gabbeh_Runner_315cm_85cm_img4.jpg" },
                    { 1, 1, "/images/products/Nur_Ilham_Faces_of_India_img1.png" },
                    { 42, 9, "/images/products/Gabbeh_Runner_315cm_85cm_img2.jpg" },
                    { 41, 9, "/images/products/Gabbeh_Runner_315cm_85cm_img1.jpg" },
                    { 37, 8, "/images/products/Ghoum_Carpet_303cm_217cm_img2.jpg" },
                    { 43, 9, "/images/products/Gabbeh_Runner_315cm_85cm_img3.jpg" },
                    { 38, 8, "/images/products/Ghoum_Carpet_303cm_217cm_img3.jpg" },
                    { 35, 7, "/images/products/Kaschmir_Carpet_230cm_145cm_img5.jpg" },
                    { 40, 8, "/images/products/Ghoum_Carpet_303cm_217cm_img5.jpg" },
                    { 68, 14, "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img3.jpg" },
                    { 67, 14, "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img2.jpg" },
                    { 66, 14, "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img1.jpg" },
                    { 30, 6, "/images/products/Sarouck_Carpet_228cm_166cm_img5.jpg" },
                    { 29, 6, "/images/products/Sarouck_Carpet_228cm_166cm_img4.jpg" },
                    { 28, 6, "/images/products/Sarouck_Carpet_228cm_166cm_img3.jpg" },
                    { 27, 6, "/images/products/Sarouck_Carpet_228cm_166cm_img2.jpg" },
                    { 26, 6, "/images/products/Sarouck_Carpet_228cm_166cm_img1.jpg" },
                    { 25, 5, "/images/products/Anton_Heyboer_Abstracte_compositie_img5.png" },
                    { 24, 5, "/images/products/Anton_Heyboer_Abstracte_compositie_img4.png" },
                    { 23, 5, "/images/products/Anton_Heyboer_Abstracte_compositie_img3.png" },
                    { 22, 5, "/images/products/Anton_Heyboer_Abstracte_compositie_img2.png" },
                    { 21, 5, "/images/products/Anton_Heyboer_Abstracte_compositie_img1.png" },
                    { 34, 7, "/images/products/Kaschmir_Carpet_230cm_145cm_img4.jpg" },
                    { 33, 7, "/images/products/Kaschmir_Carpet_230cm_145cm_img3.jpg" },
                    { 32, 7, "/images/products/Kaschmir_Carpet_230cm_145cm_img2.jpg" },
                    { 31, 7, "/images/products/Kaschmir_Carpet_230cm_145cm_img1.jpg" },
                    { 39, 8, "/images/products/Ghoum_Carpet_303cm_217cm_img4.jpg" },
                    { 20, 4, "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img5.png" },
                    { 64, 13, "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img4.jpg" },
                    { 18, 4, "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img3.png" },
                    { 74, 15, "/images/products/AuParadisdesRajahs_img4.jpg" },
                    { 73, 15, "/images/products/AuParadisdesRajahs_img3.jpg" },
                    { 72, 15, "/images/products/AuParadisdesRajahs_img2.jpg" },
                    { 71, 15, "/images/products/AuParadisdesRajahs_img1.jpg" },
                    { 50, 10, "/images/products/Bidjar_Carpet_163cm_92cm_img5.jpg" },
                    { 49, 10, "/images/products/Bidjar_Carpet_163cm_92cm_img4.jpg" },
                    { 48, 10, "/images/products/Bidjar_Carpet_163cm_92cm_img3.jpg" },
                    { 47, 10, "/images/products/Bidjar_Carpet_163cm_92cm_img2.jpg" },
                    { 75, 15, "/images/products/AuParadisdesRajahs_img5.jpg" },
                    { 46, 10, "/images/products/Bidjar_Carpet_163cm_92cm_img1.jpg" },
                    { 9, 2, "/images/products/Mata_Ni_Pachedi_img4.png" },
                    { 8, 2, "/images/products/Mata_Ni_Pachedi_img3.png" },
                    { 7, 2, "/images/products/Mata_Ni_Pachedi_img2.png" },
                    { 6, 2, "/images/products/Mata_Ni_Pachedi_img1.png" },
                    { 5, 1, "/images/products/Nur_Ilham_Faces_of_India_img5.png" },
                    { 4, 1, "/images/products/Nur_Ilham_Faces_of_India_img4.png" },
                    { 3, 1, "/images/products/Nur_Ilham_Faces_of_India_img3.png" },
                    { 2, 1, "/images/products/Nur_Ilham_Faces_of_India_img2.png" },
                    { 10, 2, "/images/products/Mata_Ni_Pachedi_img5.png" },
                    { 11, 3, "/images/products/George_Heidweiller_Zonder_titel_img1.png" },
                    { 12, 3, "/images/products/George_Heidweiller_Zonder_titel_img2.png" },
                    { 13, 3, "/images/products/George_Heidweiller_Zonder_titel_img3.png" },
                    { 17, 4, "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img2.png" },
                    { 16, 4, "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img1.png" },
                    { 65, 13, "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img5.jpg" },
                    { 69, 14, "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img4.jpg" },
                    { 63, 13, "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img3.jpg" },
                    { 62, 13, "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img2.jpg" },
                    { 61, 13, "/images/products/Les_Princesses_Malabares_ou_Le_Celibat_Phylosophique_img1.jpg" },
                    { 60, 12, "/images/products/pictoresqueIndia_img5.jpg" },
                    { 59, 12, "/images/products/pictoresqueIndia_img4.jpg" },
                    { 58, 12, "/images/products/pictoresqueIndia_img3.jpg" },
                    { 57, 12, "/images/products/pictoresqueIndia_img2.jpg" },
                    { 56, 12, "/images/products/pictoresqueIndia_img1.jpg" },
                    { 55, 11, "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img5.jpg" },
                    { 54, 11, "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img4.jpg" },
                    { 53, 11, "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img3.jpg" },
                    { 52, 11, "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img2.jpg" },
                    { 51, 11, "/images/products/Zes_jaren_uit_het_leven_van_Wemmervan_Berchem_img1.jpg" },
                    { 15, 3, "/images/products/George_Heidweiller_Zonder_titel_img5.png" },
                    { 14, 3, "/images/products/George_Heidweiller_Zonder_titel_img4.png" },
                    { 19, 4, "/images/products/Drawing_Ink_Paper_pencil_Miniatura_di_Raja_su_carta_India_img4.png" },
                    { 70, 14, "/images/products/Histoire_Des_Religions_de_tous_les_Royaumes_du_Monde_img5.jpg" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuctionHistories_ProductID",
                table: "AuctionHistories",
                column: "ProductID",
                unique: false);

            migrationBuilder.CreateIndex(
                name: "IX_AuctionHistories_UserID",
                table: "AuctionHistories",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Feedback_UserID",
                table: "Feedback",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ProductID",
                table: "Invoices",
                column: "ProductID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ShippingID",
                table: "Invoices",
                column: "ShippingID");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_UserID",
                table: "Invoices",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Photo_ProductID",
                table: "Photo",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryID",
                table: "Product",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Product_TypeID",
                table: "Product",
                column: "TypeID",
                unique: false);

            migrationBuilder.CreateIndex(
                name: "IX_Product_UserID",
                table: "Product",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_WishLists_UserID",
                table: "WishLists",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AboutUs");

            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "AuctionHistories");

            migrationBuilder.DropTable(
                name: "ContactUs");

            migrationBuilder.DropTable(
                name: "Feedback");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Photo");

            migrationBuilder.DropTable(
                name: "WishLists");

            migrationBuilder.DropTable(
                name: "Shipping");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Type");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
