﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SEM3.Controllers
{
    public class AdminController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        // GET: /<controller>/
        public IActionResult Login()
        {
            return View();
        }

        // GET: /<controller>/
        public IActionResult Profile()
        {
            return View();
        }

        // GET: /<controller>/
        public IActionResult ProfileUser()
        {
            return View();
        }

        // GET: /<controller>/
        public IActionResult ProductDetails()
        {
            return View();
        }


    }
}
